import React, { useState, useEffect } from "react";
import socketIOClient from "socket.io-client";
import logo from './logo.svg';
import './App.css';

const socket = socketIOClient("http://127.0.0.1:4001");

function App() {
  const [response, setResponse] = useState("");
  const [message, setMessage] = useState("");
  const [socketMessage, setSocketMessage] = useState("");
  const [broadcast, setBroadcast] = useState("");
  const [room, setRoom] = useState(false);

  useEffect(() => {
    socket.on("time", data => {
      setResponse(data);
    });
    socket.on("message", data => {
      setSocketMessage(data);
    });
    socket.on("broadmessage", data => {
      setBroadcast(data);
    });
  }, []);

  const sendMessage = () => {
    socket.emit("messageAll", message)
  }

  const roomMessage = () => {
    socket.emit("roomMessage", {room:room, message})
  }

  const brodacast = () => {
    socket.emit("broadcast", message)
  }

  const roomSet = (roomId) =>{
    socket.emit("subscribe",roomId)
    socket.emit("unsubscribe",roomId === "1" ? "2" : "1")
    setRoom(roomId)
  }
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {broadcast && <p> Broacasted message from socket:{broadcast}</p>}
        <button onClick={()=>roomSet("1")} disabled={room === "1"}> Room 1</button>
        <button onClick={()=>roomSet("2")} disabled={room === "2"}> Room 2</button>
        <p>
          It's <time dateTime={response}>{response}</time>
        </p>
        <input onChange={(e) => setMessage(e.target.value)}></input>
        <button onClick={sendMessage}> Submit</button>
        <p>Message from socket:{socketMessage}</p>
        <button onClick={roomMessage}> Room Submit</button>
        <button onClick={brodacast}> Send Broadcast</button>
      </header>
    </div>
  );
}

export default App;
