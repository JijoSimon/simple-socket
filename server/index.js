const express = require("express");
const http = require("http");
const socketIo = require("socket.io");

const port = process.env.PORT || 4001;
const index = require("./routes/index");

const app = express();
app.use(index);

const server = http.createServer(app);

const io = socketIo(server);

let interval;

io.on("connection", (socket) => {
  console.log("New client connected", socket.id);
  // if (interval) {
  //   clearInterval(interval);
  // }
  interval = setInterval(() => getApiAndEmit(socket), 1000);

  socket.on("messageAll", data =>{
    io.sockets.emit("message", data)
  })

  socket.on("subscribe", room=>{
    console.log("SUBS", room)
    socket.join(room)
  })

  socket.on("unsubscribe", room=>{
    console.log("SUBS", room)
    socket.leave(room)
  })

  socket.on("roomMessage", data =>{
    console.log("data",data)
    io.to(data.room).emit("message", data.message)
  })

  socket.on("broadcast", data =>{
    socket.broadcast.emit("broadmessage", data)
  })

  socket.on("disconnect", () => {
    console.log("Client disconnected");
    // clearInterval(interval);
  });
});

const getApiAndEmit = socket => {
  const response = new Date();
  // Emitting a new message. Will be consumed by the client
  socket.emit("time", response);
};

server.listen(port, () => console.log(`Listening on port ${port}`));